import mysql.connector as mysql

def conectar():
    db = mysql.connect(
    host="127.0.0.1",
    port=3306,
    user="root",
    password="secret",
    database="patrimonio")
    return db

def desconectar(db):
    db.close()