from flask import Flask, render_template, request
from model.conexao import conectar

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def entrar():
    usuario = request.form.get("usuario")
    senha = request.form.get("senha")
    if usuario:
        return render_template("dashboard.html")
    else:
        return render_template("login.html")


@app.route('/dashboard')
def dashboard():
    return render_template("dashboard.html")

@app.route('/<string:nome>')
def error(nome):
    variavel = f'Página {nome} não encontrada!' 
    return render_template("error.html", variavel=variavel)

@app.route('/sobre')
def sobre():
    return '<p>Sobre</p>'    


@app.route('/cad_usuario', methods=['GET', 'POST'])
def cad_usuario():
    usuario = request.form.get("usuario")
    senha = request.form.get("senha")
    if request.form.get("usuario"):
        db=conectar()
        cursor = db.cursor()

        cursor.execute("INSERT INTO `patrimonio`.`usuario` (usuario, senha) VALUES (%s, %s);", (usuario,senha))
        db.commit()
        return 'Usuário '+str(usuario)+' Cadastrado Com Sucesso!'
    else:
        return render_template("cad_usuario.html")
