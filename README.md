<h1>Instalar o Python3-venv</h1>
    <h2>sudo apt-get install python3-venv</h2>
<h1>Criando projeto</h1>
    <h2>python3 -m venv env</h2>
<h1>Ativando o activate do projeto</h1>
    <h2>source env/bin/activate</h2>
<h1>Como Desativar o activate</h1>
    <h2>deactivate</h2>
<h1>Inserir o caminho do arquivo que deverá ser executado ao iniciar o servidor</h1>
    <h2>export FLASK_APP=main.py</h2>
<h1>Executar o servidor</h1>
    <h2>flask run</h2>
<h1>Ativar o modo development para atualizar na medida que o código é salvo</h1>
    <h2>export FLASK_ENV=development</h2>